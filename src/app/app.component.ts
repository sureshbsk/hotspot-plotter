import { Component } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
   providers: [AngularFireDatabase]
})
export class AppComponent {

  title = 'Hotspot plotter App';


    itemValue = '';
    items: FirebaseListObservable<any[]>;

    constructor(db: AngularFireDatabase) {
      this.items = db.list('/items');
    }

    onSubmit() {
      this.items.push({content: this.itemValue});
      this.itemValue = '';
    }
}
