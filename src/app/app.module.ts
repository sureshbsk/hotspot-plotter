import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

import {environment} from '../environments/environment';

import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { HotspotComponent } from './hotspot/hotspot.component';

@NgModule({
  declarations: [
    AppComponent,
    HotspotComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot([
      {
        path:'',component:AppComponent
      },
      {
        path:'addHotspot',component:HotspotComponent
      }

    ])



  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
