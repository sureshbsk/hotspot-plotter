var mysql = require('mysql');
const express = require('express')

const bodyParser = require('body-parser');
const store = require('./store');
const app = express();

app.use(express.static('public'))
app.use(bodyParser.json())

app.post('/hotspot', (req, res) => {
  store
    .createHotspot({
      SSID: req.body.username,
      latitude: req.body.latitude,
      longitude:req.body.longitude,
      name:req.body.name
    })
    .then(() => res.sendStatus(200))
})
app.listen(8080, () => {
  console.log('Server running on http://localhost:8080')
})
